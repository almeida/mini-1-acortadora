#!/usr/bin/python3
import webapp
#import bitly_api
#BITLY_ACCESS_TOKEN ="YOUR_ACCESS_TOKEN"
#access = bitly_api.Connection(access_token ="o_16v7jdgeuv")
#full_link = input()
#short_url = access.shorten(full_link)
#print('Short URL = ',short_url['url'])
dict_url = dict()
dict_short = dict()
class contentApp (webapp.webApp):
    def parse(self, request):
        metodo=request.split(' ')[0]
        recurso=request.split(' ',2)[1]
        cuerpo=request.split('\r\n\r\n')[1]
        return metodo,cuerpo, recurso
    def process(self, request):
        global htmlBody, httpCode

        dict_url_aux=("<br>DIRECCIONES GUARDADAS <br>")
        for key in dict_url:
            dict_url_aux = dict_url_aux + "<a href='"+key+"'>"+key+"</a>" + "----->"\
                        +"<a href='"+"http://localhost:1234/"+dict_url[key]+"'>"+dict_url[key]+"</a>"+ "<br>"

        httpCode = "200 OK"
        htmlBody = "<html>" \
                   +"<title>SHORTENER.py</title>"\
                   + "<body>" \
                   + "<form method='post'>" \
                   + "<label for='URLtoShort'>URL               :  </label>" \
                   + "<input type='text' id='url'name='url'><br>" \
                   + "<label for='URLShorted'>Nombre del recurso:  </label>" \
                   + "<input type='text'id='short'name='short'><br>" \
                   + "<input type='submit'>" \
                   + "</form>" \
                   + "</body>" \
                   + "<br>" + dict_url_aux +"<br>" \
                   + "</html>"
        (metodo,cuerpo, recurso)=request
        print("metodo es "+metodo)
        print("cuerpo es "+cuerpo)
        print("recurso es "+recurso)
        if metodo == "GET":
            if recurso==("/"):
                htmlBody+="<body>"+"Dominio /"+"</body>"
                return (httpCode, htmlBody)
            else:
                siguiente=recurso.split("/",1)[1]
                print(siguiente)
                if siguiente in dict_short:
                    redireccion="<META HTTP-EQUIV='REFRESH' CONTENT='3;URL="+dict_short[siguiente]+"'"">"
                    httpCode = "404 Not Found"
                    htmlBody = "<html>" \
                               +"<title>Redirigir al navegador a otra URL</title>"\
                                 +"<body>Redirigiendo a "+dict_short[siguiente]+"</body>"\
                                + redireccion\
                               + "</html>"
                else:
                    htmlBody+="<body>"+"EL RECURSO-> ("+siguiente+") NO ES VALIDO"+"</body>"

            return (httpCode, htmlBody)

        if metodo == "POST":
            print(cuerpo)
            parteurl=cuerpo.split("&")[0]
            parteurl=parteurl.split("=")[1]
            parteshort=cuerpo.split("&")[1]
            parteshort=parteshort.split("=")[1]
            print(parteurl)
            print(len(parteurl))
            print(len(parteshort))
            if len(parteurl) == 0 and len(parteshort) == 0:
                htmlBody+="<body>"+"No has introducido nada"+"</body>"
                return (httpCode, htmlBody)
            if len(parteurl)==0:
                htmlBody+="<body>"+"Solo has introducido el nombre del recurso"+"</body>"
                return (httpCode, htmlBody)
            if len(parteshort)==0:
                htmlBody+="<body>"+"Solo has introducido la url"+"</body>"
                return (httpCode, htmlBody)

            else:
                if parteurl.find("http://") == -1 and parteurl.find("https://") == -1:
                    parteurl = "https://" + parteurl
                if parteshort in dict_short:
                    if dict_url[parteurl]==parteshort:
                        htmlBody+="<body>"+"Ese recurso ya estaba almacenado con esa url"+"<br></body>"
                    else:
                        htmlBody+="<body>"+"Ese recurso ya estaba almacenado, con otro recurso"+"<br></body>"
                        dict_url[parteurl]=parteshort
                        dict_short[parteshort]=parteurl
                else:
                    dict_url[parteurl]=parteshort
                    dict_short[parteshort]=parteurl
                    htmlBody+="<body>"+"Guardando(submit para actualizar): "+parteurl+"->"+parteshort+"</body>"
                return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)